package main.java.org.Intervale.functions;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class MathFunctions {

    public static String round(String value) {
        int places = 5;
        if (places < 0) throw new IllegalArgumentException();
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return String.valueOf(bd);
    }

    public static double sum(double x, double y) {
        return x + y; //возвращает результат сложения перменной х и у
    }

    public static double substraction(double x, double y) {
        return x - y;//возвращает результат вычитания перменной х и у
    }

    public static double division(double x, double y) {

        return x / y;//возвращает результат деления перменной х на у
    }

    public static double multiplication(double x, double y) {
        return x * y;//возвращает результат умножения перменной х на у
    }

    public static double exponentiation(double x, int y) {
        return Math.pow(x, y);//возвращает результат возведения х в степень у
    }

    public static double percent(double x, double y) {
        return (x / 100) * y;//возвращает процент y от числа х
    }

}
