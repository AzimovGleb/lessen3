package main.java.org.Intervale.calculator;

import main.java.org.Intervale.functions.MathFunctions;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CalculatorImpl implements Calculator {

    @Override
    public String run(String nameFileRead, String nameFileWrite) {
        List<String> list = new ArrayList<String>();
        String[] array = null;
        int error = 0;
        int veriablesCounter = 0;
        Pattern patternOperation = Pattern.compile("(-?\\d+\\.?\\d*)?(\\S)(-?\\d+\\.?\\d*)");
        ArrayList<String> veriablesList = new ArrayList<String>();
        HashMap<String, String> veriablesMap = new HashMap<String, String>();

        array = read(nameFileRead);
        try (FileWriter writer = new FileWriter(nameFileWrite)) { //файл(для записи)
            for (String n : array) {
                veriablesCounter = 0;
                Pattern patternVeriables = Pattern.compile("[\\=]");
                Matcher matcherVeriables = patternVeriables.matcher(n);
                if (matcherVeriables.find() && veriablesMap.size() <= 0) {
                    veriablesCounter++;
                    veriablesList = makeCalculationsWithoutVeriables(n);
                    n = veriablesList.get(1);
                } else if (veriablesMap.size() > 0) {
                    veriablesCounter++;
                    n = replacementOfVariablesInCalculation(veriablesMap, makeCalculationsWithoutVeriables(n));
                }

                ArrayList<String> operatorsList = parsOperators(n);//массив операторов
                ArrayList<String> operandsList = parsOperands(n);// массив операндов
                error = 0;
                Matcher matcherOperation = patternOperation.matcher(n);

                if (matcherOperation.find()) {
                    while (operandsList.size() > 1) {
                        int i = getOperatorWithMaxPriority(operatorsList);
                        if (i == -1) return ("Exception. Operation not supported");
                        operandsList = checkDivisionByZero(i, operandsList, operatorsList);
                        if (operandsList.get(i).equals("Division by zero")) {
                            error++;
                            break;
                        }
                        String resultOperation = performOperation(operandsList.get(i), operandsList.get(i + 1), operatorsList.get(i));
                        operandsList.remove(i);
                        operandsList.remove(i);
                        operandsList.add(i, resultOperation);
                        operatorsList.remove(i);
                    }
                }

                operandsList = roundResult(operandsList);
                if (error > 0) {
                    writer.write(operandsList.get(0) + "\r\n");
                } else if (veriablesCounter > 0) {
                    writer.write(operandsList.get(0) + "\r\n");
                    veriablesMap.put(veriablesList.get(0), operandsList.get(0));
                } else {
                    writer.write(n + "=" + operandsList.get(0) + "\r\n");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "Calculations completed successfully";
    }

    @Override
    public ArrayList<String> checkDivisionByZero(int i, ArrayList<String> operandsList,
                                                 ArrayList<String> operatorsList) {
        if (operatorsList.get(i).equals("/")) {
            double numError = Double.parseDouble(operandsList.get(i + 1));
            if (numError == 0) {
                operandsList.set(i, "Division by zero");
            }
        }
        return operandsList;
    }

    @Override
    public String[] read(String fileName) {
        List<String> list = new ArrayList<String>();
        String[] array = null;
        try (Scanner in = new Scanner(new File(fileName))) { //файл(для чтения)

            while (in.hasNextLine()) {
                list.add(in.nextLine());
                array = list.toArray(new String[list.size()]);
                //ввод в массив
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return array;
    }

    @Override
    public int getOperatorWithMaxPriority(List<String> operators) {
        String[] oper = {"%", "^", "/", "*", "-", "+"};
        for (String i : oper) {
            for (String j : operators) {
                if (j.equals(i)) {
                    int ret = operators.indexOf(j);
                    return ret;
                }
            }
        }
        return -1;
    }

    @Override
    public ArrayList<String> roundResult(ArrayList<String> operandsList) {
        Pattern numPatWithNull = Pattern.compile("(^-?[\\d]+[\\.][\\d]$)");
        Matcher matcherNumWithNull = numPatWithNull.matcher(operandsList.get(0));
        Pattern numPat = Pattern.compile("(^-?[\\d]+[\\.][\\d]+$)");
        Matcher matcherNum = numPat.matcher(operandsList.get(0));
        if (matcherNumWithNull.find()) {
            String[] roundNums = operandsList.get(0).split("\\.");
            if (roundNums[1].equals("0")) operandsList.set(0, roundNums[0]);
        } else if (matcherNum.find()) {
            operandsList.set(0, MathFunctions.round(operandsList.get(0)));
        }
        return operandsList;
    }

    @Override
    public ArrayList<String> parsOperands(String s) {
        String[] operands = s.split("([\\^\\*\\-\\+\\/\\%])");
        ArrayList<String> operandsList = new ArrayList<String>(Arrays.asList(operands));
        return operandsList;
    }

    @Override
    public ArrayList<String> parsOperators(String s) {
        String[] operators = s.split("[\\d]+\\.?[\\d]*");
        ArrayList<String> operatorsList = new ArrayList<>(Arrays.asList(operators));//массив операторов
        operatorsList.remove(0);
        return operatorsList;
    }

    @Override
    public String performOperation(String numOne, String numTwo, String operator) {
        double result = 0;
        switch (operator) {
            case "%":
                result = MathFunctions.percent(Double.parseDouble(numOne), Double.parseDouble(numTwo));
                break;

            case "^":
                result = MathFunctions.exponentiation(Double.parseDouble(numOne), Integer.parseInt(numTwo));
                break;

            case "+":
                result = MathFunctions.sum(Double.parseDouble(numOne), Double.parseDouble(numTwo));
                break;

            case "-":
                result = MathFunctions.substraction(Double.parseDouble(numOne), Double.parseDouble(numTwo));
                break;

            case "*":
                result = MathFunctions.multiplication(Double.parseDouble(numOne), Double.parseDouble(numTwo));
                break;

            case "/":
                result = MathFunctions.division(Double.parseDouble(numOne), Double.parseDouble(numTwo));
                break;

        }
        return String.valueOf(result);
    }

    @Override
    public String replacementOfVariablesInCalculation(Map<String, String> veriablesMap,
                                                      ArrayList<String> veriablesList) {
        String calculationsWithVariables = veriablesList.get(1);
        String[] calculationsWithVariablesList = calculationsWithVariables.split("");
        StringBuilder trueCalculations = new StringBuilder();
        for (String maybeVariables : calculationsWithVariablesList) {
            if (veriablesMap.containsKey(maybeVariables) == true) {
                maybeVariables = String.valueOf(veriablesMap.get(maybeVariables));//замена переменной на её значение из списка
                trueCalculations.append(maybeVariables);
            } else {
                trueCalculations.append(maybeVariables);
            }
        }
        String n = trueCalculations.toString();
        for (String key : veriablesMap.keySet()) {
            if (key.equals(veriablesList.get(0))) veriablesMap.remove(key);
        }
        return n;
    }

    @Override
    public ArrayList<String> makeCalculationsWithoutVeriables(String calculationWithVeriables) {
        String[] veriables = calculationWithVeriables.split("=");
        ArrayList<String> list = new ArrayList<>(Arrays.asList(veriables));
        return list;
    }
}