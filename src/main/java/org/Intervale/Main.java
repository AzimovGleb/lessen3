package main.java.org.Intervale;

import main.java.org.Intervale.calculator.Calculator;
import main.java.org.Intervale.calculator.CalculatorImpl;

public class Main {

    public static void main(String[] args) {
        Calculator calc = new CalculatorImpl();
        System.out.println(calc.run("src\\main\\resources\\intput_1.txt", "src\\main\\resources\\output_1.txt"));
    }
}
