package main.java.org.Intervale.calculator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface Calculator {

    String run(String nameFileRead, String nameFileWrite);

    int getOperatorWithMaxPriority(List<String> operators);

    String performOperation(String numOne, String numTwo, String operator);

    String replacementOfVariablesInCalculation(Map<String, String> veriablesMap, ArrayList<String> veriablesList);

    ArrayList<String> makeCalculationsWithoutVeriables(String calculationWithVeriables);

    ArrayList<String> roundResult(ArrayList<String> operandsList);

    ArrayList<String> parsOperands(String s);

    ArrayList<String> parsOperators(String s);

    String[] read(String fileName);

    ArrayList<String> checkDivisionByZero(int i, ArrayList<String> operandsList, ArrayList<String> operatorsList);
}
